package com.iluwatar.abstractfactory.factory;

public enum KingdomType {

    ELF, ORC
}
